// @ts-check

/**
 * @private
 * @typedef type_option_callback_data
 * @property {string} [mode] mode work cashed callback data subsystem - 'manual', 'all', 'auto', default - 'manual'
 * @property {string} [path] path for save cache for work with cashed callback data subsystem
 * @property {boolean} [allow_bot_name_in_path] add to option "path" bot name, default - true
 * @property {number} [timelive] timelive cache for cashed callback data, in minutes
 */

/**
 * @private
 * @typedef type_option_message
 * @property {boolean} [mode] default mode work cashed message subsystem - default - false
 * @property {string} [path] path for save message object (text, callback data, inline keyboard, ext.)
 * @property {boolean} [allow_bot_name_in_path] add to option "path" bot name, default - true
 * @property {number} [timelive] timelive cache for cashed message, in minutes
 */

/** @private */
const lib_vconv = require('viva-convert')
/** @private */
const lib_telegram = require('./index.js')
/** @private */
const lib_path = require('path')
/** @private */
const lib_fs = require('fs-extra')
/** @private */
const lib_util = require('util')
/** @private */
const lib_event = require('events').EventEmitter

module.exports = Cashe
lib_util.inherits(Cashe, lib_event)
Cashe.prototype.emit = Cashe.prototype.emit || undefined
Cashe.prototype.on = Cashe.prototype.on || undefined


function Cashe() {
    if (!(this instanceof Cashe)) return new Cashe()
    //TODO wtf? in one comp error, in another comp no error (
    // @ts-ignore
    lib_event.call(this)
}

/** @type {lib_telegram} */
Cashe.prototype.telegram = undefined
/** @type {type_option_callback_data} */
Cashe.prototype.option_callback_data = undefined
/** @type {type_option_message} */
Cashe.prototype.option_message = undefined

/**
 * @param {lib_telegram} telegram
 * @param {type_option_callback_data} option_callback_data
 * @param {type_option_message} option_message
 */
Cashe.prototype.init = function (telegram, option_callback_data, option_message) {
    this.telegram = telegram
    this.option_callback_data = option_callback_data
    this.option_message = option_message
    this.cashe_callback_data_clear()
    this.cashe_message_clear()
}

/**
 * @returns {string}
 */
Cashe.prototype.cashe_callback_data_path = function () {
    let path = this.option_callback_data.path
    if (this.option_callback_data.allow_bot_name_in_path !== false) {
        path = lib_path.join(path, this.telegram.bot_name)
    }
    return lib_path.join(path, 'callback_data')
}

/**
 * @returns {string}
 */
Cashe.prototype.cashe_message_path = function () {
    let path = this.option_message.path
    if (this.option_message.allow_bot_name_in_path !== false) {
        path = lib_path.join(path, this.telegram.bot_name)
    }
    return lib_path.join(path, 'message')
}

/**
 * @param {string} account
 * @param {number} message_id
 * @param {string} key
 * @returns {string}
 */
Cashe.prototype.cashe_callback_data_filename = function (account, message_id, key) {
    if (lib_vconv.isAbsent(account) || lib_vconv.isAbsent(message_id) || lib_vconv.isAbsent(key)) {
        return undefined
    }
    let path = this.cashe_callback_data_path()
    let date = key.substring(5, 13)
    let index = key.substring(14, key.length - 1)
    return lib_path.join(path, date, account, message_id.toString().concat('_',index,'.json'))
}

/**
 * @param {string} account
 * @param {number} message_id
 * @returns {string}
 */
Cashe.prototype.cashe_message_filename = function (account, message_id) {
    if (lib_vconv.isAbsent(account) || lib_vconv.isAbsent(message_id)) {
        return undefined
    }
    let path = this.cashe_message_path()
    return lib_path.join(path, lib_vconv.formatDate(new Date(), 112) , account, message_id.toString().concat('.json'))
}

Cashe.prototype.cashe_callback_data_clear = function () {
    if (lib_vconv.isEmpty(this.option_callback_data.path)) return
    let timelive = lib_vconv.toInt(this.option_callback_data.timelive, 0)
    if (timelive <= 0) return
    let chunk = 3
    if (timelive <= 60) {
        chunk = 10
    } else if (timelive <= 720) {
        chunk = 5
    }
    let timeout = Math.floor(timelive / chunk)
    if (timeout <= 0) {
        timeout = 1
    }

    let self = this
    let timer = setTimeout(function tick() {
        try {
            let root_path = self.cashe_callback_data_path()
            let days_leave = []
            let d = new Date()
            let max_d = lib_vconv.toDate(lib_vconv.formatDate(d, 112))
            let min_d = lib_vconv.toDate(lib_vconv.formatDate(lib_vconv.dateAdd('minute', -1 * timelive, d), 112))

            while (max_d >= min_d) {
                days_leave.push(lib_vconv.formatDate(min_d,112))
                min_d = lib_vconv.dateAdd('day', 1, min_d)
            }

            let min_dt = lib_vconv.dateAdd('minute', -1 * timelive, d)

            lib_fs.exists(root_path, exists => {

                if (exists === true) {
                    lib_fs.readdir(root_path, (error, files) => {
                        if (!lib_vconv.isEmpty(error)) {
                            self.emit('error', error)
                        } else {
                            files.forEach(file => {
                                if (!days_leave.includes(file)) {
                                    lib_fs
                                        .remove(lib_path.join(root_path, file))
                                        .catch(error => {
                                            self.emit('error', error)
                                        })
                                }
                            })
                        }
                    })

                    let path = lib_path.join(root_path, days_leave[0])
                    lib_fs.exists(path, exists => {
                        if (exists === true) {
                            scan_path(path, (error, result) => {
                                if (!lib_vconv.isEmpty(error)) {
                                    self.emit('error', error)
                                } else {
                                    result.forEach(file => {
                                        if (file.stat.birthtime < min_dt || file.stat.mtime < min_dt) {
                                            lib_fs.unlink(file.file, error => {
                                                if (!lib_vconv.isEmpty(error)) {
                                                    self.emit('error', error)
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    })
                }

                timer = setTimeout(tick, timeout * 1000 * 60)
            })
        } catch (error) {
            self.emit('error', error)
        }
    }, 1000)
}

Cashe.prototype.cashe_message_clear = function () {
    if (lib_vconv.isEmpty(this.option_message.path)) return
    let timelive = lib_vconv.toInt(this.option_message.timelive, 0)
    if (timelive <= 0) return
    let chunk = 3
    if (timelive <= 60) {
        chunk = 10
    } else if (timelive <= 720) {
        chunk = 5
    }
    let timeout = Math.floor(timelive / chunk)
    if (timeout <= 0) {
        timeout = 1
    }

    let self = this
    let timer = setTimeout(function tick() {
        try {
            let root_path = self.cashe_message_path()
            let days_leave = []
            let d = new Date()
            let max_d = lib_vconv.toDate(lib_vconv.formatDate(d, 112))
            let min_d = lib_vconv.toDate(lib_vconv.formatDate(lib_vconv.dateAdd('minute', -1 * timelive, d), 112))

            while (max_d >= min_d) {
                days_leave.push(lib_vconv.formatDate(min_d,112))
                min_d = lib_vconv.dateAdd('day', 1, min_d)
            }

            let min_dt = lib_vconv.dateAdd('minute', -1 * timelive, d)

            lib_fs.exists(root_path, exists => {

                if (exists === true) {
                    lib_fs.readdir(root_path, (error, files) => {
                        if (!lib_vconv.isEmpty(error)) {
                            self.emit('error', error)
                        } else {
                            files.forEach(file => {
                                if (!days_leave.includes(file)) {
                                    lib_fs
                                        .remove(lib_path.join(root_path, file))
                                        .catch(error => {
                                            self.emit('error', error)
                                        })
                                }
                            })
                        }
                    })

                    let path = lib_path.join(root_path, days_leave[0])
                    lib_fs.exists(path, exists => {
                        if (exists === true) {
                            scan_path(path, (error, result) => {
                                if (!lib_vconv.isEmpty(error)) {
                                    self.emit('error', error)
                                } else {
                                    result.forEach(file => {
                                        if (file.stat.birthtime < min_dt || file.stat.mtime < min_dt) {
                                            lib_fs.unlink(file.file, error => {
                                                if (!lib_vconv.isEmpty(error)) {
                                                    self.emit('error', error)
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    })
                }

                timer = setTimeout(tick, timeout * 1000 * 60)
            })
        } catch (error) {
            self.emit('error', error)
        }
    }, 3000)
}

/**
 * @param {string} account
 * @param {number} message_id
 * @param {string} callback_data
 * @param {function} callback (error, result)
 */
Cashe.prototype.cashe_callback_data_load = function (account, message_id, callback_data, callback) {
    if (!lib_vconv.isFunction(callback)) {
        throw new Error('callback is not function')
    }
    try {
        if (lib_vconv.isEmpty(account) || lib_vconv.isEmpty(message_id) || lib_vconv.isEmpty(callback_data) || callback_data.substring(0,5) !== '#ccd#') {
            callback(undefined, callback_data)
            return
        }
        let file = this.cashe_callback_data_filename(account, message_id, callback_data)
        if (lib_vconv.isEmpty(file)) {
            callback(undefined, callback_data)
            return
        }
        lib_fs.exists(file, exists => {
            if (exists === false) {
                callback(undefined, callback_data)
                return
            }
            lib_fs.readFile(file, 'utf8', (error, data) => {
                if (!lib_vconv.isEmpty(error)) {
                    callback(error)
                    return
                }
                callback(undefined, data)
            })
        })
    } catch (error) {
        callback(error)
    }
}

/**
 * @param {string} account
 * @param {number} message_id
 * @param {function} callback (error, result = {message, buttons, option, image})
 */
Cashe.prototype.cashe_message_load = function (account, message_id, callback) {
    if (!lib_vconv.isFunction(callback)) {
        throw new Error('callback is not function')
    }
    try {
        let root_path = this.cashe_message_path()
        lib_fs.exists(root_path, exists => {
            if (exists !== true) {
                callback(undefined)
                return
            }
            lib_fs.readdir(root_path, (error, files) => {
                if (!lib_vconv.isEmpty(error)) {
                    callback(error)
                    return
                }
                let find = false
                files.sort((a,b) => (a > b ? -1 : 1)).forEach(date_path => {
                    let find_file = lib_path.join(root_path, date_path, account, message_id.toString().concat('.json'))
                    if (lib_fs.existsSync(find_file) === true) {
                        find = true
                        lib_fs.readFile(find_file, 'utf8', (error, data) => {
                            if (!lib_vconv.isEmpty(error)) {
                                callback(error)
                                return
                            }
                            if (lib_vconv.isEmpty(data)) {
                                callback(undefined)
                                return
                            }
                            try {
                                let message = JSON.parse(data)
                                callback (undefined, {
                                    message: message.message,
                                    buttons: message.buttons,
                                    option: message.option,
                                    image: (lib_vconv.isAbsent(message.image) ? undefined : Buffer.from(message.image,'base64'))
                                })
                                return
                            } catch (error) {
                                callback(error)
                                return
                            }
                        })
                        return
                    }
                })
                if (find === false) {
                    callback(undefined)
                }
            })
        })
    } catch (error) {
        callback(error)
    }
}

/**
 * @param {string} message
 * @param {Object} buttons
 * @param {lib_telegram.type_send_message_option} option
 * @param {Buffer} [image]
 * @returns {string}
 */
Cashe.prototype.cashe_message_build = function (message, buttons, option, image) {
    if ((!lib_vconv.isAbsent(option) && option.cashe_message === true) || this.option_message.mode === true) {
        if (lib_vconv.isEmpty(this.option_message.path)) {
            throw new Error ('in send message cashe_message = true, but cashe_message_path is empty')
        }
        return JSON.stringify ({
            message: message,
            buttons: buttons,
            option: option,
            image: (lib_vconv.isAbsent(image) ? undefined : image.toString('base64'))
        }, null, '\t')
    }
    return undefined
}

/**
 * @typedef type_cashe_callback_data_build
 * @property {string} key
 * @property {string} data
 *
 * @param {Array.<Array.<lib_telegram.type_send_button>>} buttons
 * @returns {type_cashe_callback_data_build[]}
 */
Cashe.prototype.cashe_callback_data_build = function (buttons) {
    if (lib_vconv.isAbsent(buttons)) {
        return undefined
    }
    let d = new Date()
    let i = 0
    /** @type {type_cashe_callback_data_build[]} */
    let result = []

    buttons.filter(f => !lib_vconv.isAbsent(f)).forEach(b1 => {
        b1.filter(f => !lib_vconv.isAbsent(f)).forEach(b2 => {
            if (typeof b2.callback_data === 'object') {
                b2.callback_data = JSON.stringify(b2.callback_data)
            }
            if (lib_vconv.toString(b2.callback_data,'') === '') {
                b2.callback_data = ' '
            }
            if (
                (b2.cashe_callback_data === true) ||
                (this.option_callback_data.mode === 'all' && b2.cashe_callback_data !== false) ||
                (this.option_callback_data.mode === 'auto' && b2.cashe_callback_data !== false && Buffer.byteLength(b2.callback_data, 'utf8') > 64)
                ) {
                if (lib_vconv.isEmpty(this.option_callback_data.path)) {
                    throw new Error ('in send message cashe_callback_data = true, but cashe_callback_data_path is empty')
                }
                let key = '#ccd#'.concat(lib_vconv.formatDate(d, 112),':', i.toString(),'#')
                i++
                result.push({
                    key: key,
                    data: b2.callback_data
                })
                b2.callback_data = key
            }
        })
    })

    return result
}

/**
 *
 * @param {string} account
 * @param {number} message_id
 * @param {string} message_cashe
 * @param {type_cashe_callback_data_build[]} callback_data_cashe
 * @param {function} callback error
 */
Cashe.prototype.save = function (account, message_id, message_cashe, callback_data_cashe, callback) {
    if (!lib_vconv.isFunction(callback)) {
        throw new Error('callback is not function')
    }
    try {
        /** @type {type_writefiles[]} */
        let files = []

        if (!lib_vconv.isAbsent(callback_data_cashe) && callback_data_cashe.length > 0) {
            callback_data_cashe.forEach(d => {
                files.push({
                    file_name: this.cashe_callback_data_filename(account, message_id, d.key),
                    file_data: d.data
                })
            })
        }

        if (!lib_vconv.isEmpty(message_cashe)) {
            files.push({
                file_name: this.cashe_message_filename(account, message_id),
                file_data: message_cashe
            })
        }

        if (files.length <= 0) {
            callback(undefined)
            return
        }

        write_files(files, error => {
            callback(error)
        })

    } catch (error) {
        callback (error)
    }
}

function scan_path(dir, callback) {
    var results = [];
    lib_fs.readdir(dir, function(err, list) {
        if (err) return callback(err)
        var pending = list.length
        if (!pending) return callback(undefined, results)
            list.forEach(function(file) {
            file = lib_path.resolve(dir, file)
            lib_fs.stat(file, function(err, stat) {
                if (stat && stat.isDirectory()) {
                    scan_path(file, function(err, res) {
                    results = results.concat(res)
                    if (!--pending) callback(undefined, results)
                    })
                } else {
                    results.push({file: file, stat: stat})
                    if (!--pending) callback(undefined, results)
                }
            })
        })
    })
}

/**
 * @typedef type_writefiles
 * @property {string} file_name
 * @property {string} file_data
 *
 * @param {type_writefiles[]} files
 * @param {Function} [callback] error
 */
function write_files (files, callback) {
    try {
        write_files_internal(files, 0, error => {
            if (lib_vconv.isFunction(callback)) {
                callback(error)
            }
        })
    } catch (error) {
        if (lib_vconv.isFunction(callback)) {
            callback(error)
        }
    }
}

/**
 * @private
 * @param {type_writefiles[]} files
 * @param {number} index
 * @param {Function} callback
 */
function write_files_internal (files, index, callback) {
    if (files.length > index) {
        if (lib_vconv.isEmpty(files[index].file_name)) {
            index++
            write_files_internal(files, index, callback)
            return
        }
        let path = lib_path.dirname(files[index].file_name)
        if (!lib_fs.existsSync(path)) {
            lib_fs.mkdirSync(path, {recursive: true})
        }
        lib_fs.writeFile(files[index].file_name, files[index].file_data, error => {
            if (!lib_vconv.isEmpty(error)) {
                callback(error)
                return
            }
            index++
            write_files_internal(files, index, callback)
        })
    } else {
        callback(undefined)
    }
}