## Classes

<dl>
<dt><a href="#Telegram">Telegram</a></dt>
<dd><p>(license MIT) library for work with telegram, full example - see example.js</p>
</dd>
</dl>

## Typedefs

<dl>
<dt><a href="#type_option">type_option</a></dt>
<dd></dd>
<dt><a href="#type_send_keyboardmenu_option">type_send_keyboardmenu_option</a></dt>
<dd></dd>
<dt><a href="#type_send_message_option">type_send_message_option</a></dt>
<dd></dd>
<dt><a href="#type_send_image_option">type_send_image_option</a></dt>
<dd></dd>
<dt><a href="#type_send_button">type_send_button</a></dt>
<dd></dd>
<dt><a href="#type_send_queue_message">type_send_queue_message</a></dt>
<dd></dd>
</dl>

<a name="Telegram"></a>

## Telegram
(license MIT) library for work with telegram, full example - see example.js

**Kind**: global class  

* [Telegram](#Telegram)
    * [.bot_name](#Telegram+bot_name) : <code>string</code>
    * [.cache](#Telegram+cache) : <code>lib\_cache</code>
    * [.message_text_markdown_escape(text)](#Telegram+message_text_markdown_escape) ⇒ <code>string</code>
    * [.launch(token, [option], callback)](#Telegram+launch)
    * [.keyboard(notice, message, keyboard, [option], [callback])](#Telegram+keyboard)
    * [.message(notice, message, [buttons], [option], [callback])](#Telegram+message)
    * [.image(notice, image, [message], [buttons], [option], [callback])](#Telegram+image)
    * [.delete(notice, message_id, [callback])](#Telegram+delete)
    * [.load_from_cashe(account, message_id, callback)](#Telegram+load_from_cashe)
    * [.queue(notice, items, [callback])](#Telegram+queue)

<a name="Telegram+bot_name"></a>

### telegram.bot\_name : <code>string</code>
**Kind**: instance property of [<code>Telegram</code>](#Telegram)  
<a name="Telegram+cache"></a>

### telegram.cache : <code>lib\_cache</code>
**Kind**: instance property of [<code>Telegram</code>](#Telegram)  
<a name="Telegram+message_text_markdown_escape"></a>

### telegram.message\_text\_markdown\_escape(text) ⇒ <code>string</code>
escaping service markdown characters

**Kind**: instance method of [<code>Telegram</code>](#Telegram)  

| Param | Type |
| --- | --- |
| text | <code>string</code> | 

<a name="Telegram+launch"></a>

### telegram.launch(token, [option], callback)
main function for launch bot

**Kind**: instance method of [<code>Telegram</code>](#Telegram)  

| Param | Type | Description |
| --- | --- | --- |
| token | <code>string</code> |  |
| [option] | [<code>type\_option</code>](#type_option) |  |
| callback | <code>function</code> | error |

<a name="Telegram+keyboard"></a>

### telegram.keyboard(notice, message, keyboard, [option], [callback])
send message and create keyboard, located under the text entry field

**Kind**: instance method of [<code>Telegram</code>](#Telegram)  

| Param | Type | Description |
| --- | --- | --- |
| notice | <code>type\_notice</code> |  |
| message | <code>string</code> |  |
| keyboard | <code>Array.&lt;Array.&lt;type\_send\_keyboardmenu\_option&gt;&gt;</code> |  |
| [option] | <code>type\_send\_keyboard\_option</code> |  |
| [callback] | <code>function</code> | message_id - number |

<a name="Telegram+message"></a>

### telegram.message(notice, message, [buttons], [option], [callback])
send message with buttons below this message

**Kind**: instance method of [<code>Telegram</code>](#Telegram)  

| Param | Type | Description |
| --- | --- | --- |
| notice | <code>type\_notice</code> |  |
| message | <code>string</code> |  |
| [buttons] | <code>Array.&lt;Array.&lt;type\_send\_button&gt;&gt;</code> |  |
| [option] | [<code>type\_send\_message\_option</code>](#type_send_message_option) |  |
| [callback] | <code>function</code> | message_id - number |

<a name="Telegram+image"></a>

### telegram.image(notice, image, [message], [buttons], [option], [callback])
send image with buttons below this message

**Kind**: instance method of [<code>Telegram</code>](#Telegram)  

| Param | Type | Description |
| --- | --- | --- |
| notice | <code>type\_notice</code> |  |
| image | <code>Buffer</code> |  |
| [message] | <code>string</code> |  |
| [buttons] | <code>Array.&lt;Array.&lt;type\_send\_button&gt;&gt;</code> |  |
| [option] | [<code>type\_send\_image\_option</code>](#type_send_image_option) |  |
| [callback] | <code>function</code> | message_id - number |

<a name="Telegram+delete"></a>

### telegram.delete(notice, message_id, [callback])
delete previously sent message or image

**Kind**: instance method of [<code>Telegram</code>](#Telegram)  

| Param | Type | Description |
| --- | --- | --- |
| notice | <code>type\_notice</code> |  |
| message_id | <code>number</code> |  |
| [callback] | <code>function</code> | is_deleted - boolean |

<a name="Telegram+load_from_cashe"></a>

### telegram.load\_from\_cashe(account, message_id, callback)
from cashe load previously saved message or image

**Kind**: instance method of [<code>Telegram</code>](#Telegram)  

| Param | Type | Description |
| --- | --- | --- |
| account | <code>string</code> |  |
| message_id | <code>number</code> |  |
| callback | <code>function</code> | (error, result = {message, buttons, option, image}) |

<a name="Telegram+queue"></a>

### telegram.queue(notice, items, [callback])
send many messages

**Kind**: instance method of [<code>Telegram</code>](#Telegram)  

| Param | Type | Description |
| --- | --- | --- |
| notice | <code>type\_notice</code> |  |
| items | [<code>Array.&lt;type\_send\_queue\_message&gt;</code>](#type_send_queue_message) |  |
| [callback] | <code>function</code> | message_ids - number[] |

<a name="type_option"></a>

## type\_option
**Kind**: global typedef  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| [cashe_callback_data] | <code>string</code> | cashed callback data subsystem: default mode work (can be specified explicitly in each message), variants - 'manual', 'all', 'auto', default - 'manual' |
| [cashe_callback_data_path] | <code>string</code> | cashed callback data subsystem: path for save cache |
| [cashe_callback_data_path_bot_name] | <code>boolean</code> | cashed callback data subsystem: add to option "cashe_callback_data_path" bot name, default - true |
| [cashe_callback_data_timelive] | <code>number</code> | cashed callback data subsystem: timelive for cashed data live, in minutes |
| [cashe_message] | <code>boolean</code> | cashed message subsystem: default mode work (can be specified explicitly in each message) - default - false |
| [cashe_message_path] | <code>string</code> | cashed message subsystem: path for save cache |
| [cashe_message_path_bot_name] | <code>boolean</code> | cashed message subsystem: add to option "cashe_message_path" bot name, default - true |
| [cashe_message_timelive] | <code>number</code> | cashed message subsystem: timelive for cashed data live, in minutes |

<a name="type_send_keyboardmenu_option"></a>

## type\_send\_keyboardmenu\_option
**Kind**: global typedef  
**Properties**

| Name | Type |
| --- | --- |
| text | <code>string</code> | 
| [is_request_contact] | <code>boolean</code> | 
| [is_request_location] | <code>boolean</code> | 

<a name="type_send_message_option"></a>

## type\_send\_message\_option
**Kind**: global typedef  
**Properties**

| Name | Type |
| --- | --- |
| [is_message_format_markdown] | <code>boolean</code> | 
| [is_keyboard_remove] | <code>boolean</code> | 
| [is_reply_to_message_from_notice] | <code>boolean</code> | 
| [cashe_message] | <code>boolean</code> | 

<a name="type_send_image_option"></a>

## type\_send\_image\_option
**Kind**: global typedef  
**Properties**

| Name | Type |
| --- | --- |
| [is_message_format_markdown] | <code>boolean</code> | 
| [is_keyboard_remove] | <code>boolean</code> | 
| [is_reply_to_message_from_notice] | <code>boolean</code> | 
| [cashe_message] | <code>boolean</code> | 

<a name="type_send_button"></a>

## type\_send\_button
**Kind**: global typedef  
**Properties**

| Name | Type |
| --- | --- |
| text | <code>string</code> | 
| [callback_data] | <code>string</code> \| <code>Object</code> | 
| [cashe_callback_data] | <code>boolean</code> | 

<a name="type_send_queue_message"></a>

## type\_send\_queue\_message
**Kind**: global typedef  
**Properties**

| Name | Type |
| --- | --- |
| message | <code>string</code> | 
| [buttons] | <code>Array.&lt;Array.&lt;type\_send\_button&gt;&gt;</code> | 
| [option] | [<code>type\_send\_message\_option</code>](#type_send_message_option) | 

