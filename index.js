// @ts-check
/**
 * @license MIT
 * @author Vitalii Vasilev
 */

/**
 * @private
 * @typedef type_need_upload_document
 * @property {string} file_id
 * @property {string} file_name
 * @property {number} file_size
 * @property {string} mime_type
 */

/**
 * @typedef type_option
 * @property {string} [cashe_callback_data] cashed callback data subsystem: default mode work (can be specified explicitly in each message), variants - 'manual', 'all', 'auto', default - 'manual'
 * @property {string} [cashe_callback_data_path] cashed callback data subsystem: path for save cache
 * @property {boolean} [cashe_callback_data_path_bot_name] cashed callback data subsystem: add to option "cashe_callback_data_path" bot name, default - true
 * @property {number} [cashe_callback_data_timelive] cashed callback data subsystem: timelive for cashed data live, in minutes
 * @property {boolean} [cashe_message] cashed message subsystem: default mode work (can be specified explicitly in each message) - default - false
 * @property {string} [cashe_message_path] cashed message subsystem: path for save cache
 * @property {boolean} [cashe_message_path_bot_name] cashed message subsystem: add to option "cashe_message_path" bot name, default - true
 * @property {number} [cashe_message_timelive] cashed message subsystem: timelive for cashed data live, in minutes
 */

/**
 * @private
 * @typedef type_notice
 * @property {Object} [_ctx]
 * @property {boolean} [is_bot]
 * @property {string} account
 * @property {string} [language]
 * @property {number} [chat_id]
 * @property {number} [message_id]
 * @property {string} [message]
 * @property {number} [location_latitude]
 * @property {number} [location_longitude]
 * @property {number} [utc_offset]
 * @property {string} [utc_offset_name]
 * @property {string} [callback_data]
 * @property {type_need_upload_document} [need_upload_document]
 */

/**
 * @private
 * @typedef type_send_text_option
 * @property {string} [parse_mode] //'Markdown','HTML'
 * @property {boolean} [disable_notification]
 * @property {boolean} [disable_web_page_preview]
 * @property {number} [reply_to_message_id]
 * @property {any} [reply_markup]
 */

/**
 * @private
 * @typedef type_send_photo_option
 * @property {string} [parse_mode] //'Markdown','HTML'
 * @property {boolean} [disable_notification]
 * @property {number} [reply_to_message_id]
 * @property {any} [reply_markup]
 * @property {string} [caption]
 */

/**
 * @private
 * @typedef type_send_keyboard_option
 * @property {boolean} [is_caption_format_markdown]
 * @property {boolean} [is_keyboard_hide_after_click]
 * @property {boolean} [is_reply_to_message_from_notice]
 */

/**
 * @typedef type_send_keyboardmenu_option
 * @property {string} text
 * @property {boolean} [is_request_contact]
 * @property {boolean} [is_request_location]
 */

/**
 * @typedef type_send_message_option
 * @property {boolean} [is_message_format_markdown]
 * @property {boolean} [is_keyboard_remove]
 * @property {boolean} [is_reply_to_message_from_notice]
 * @property {boolean} [cashe_message]
 */

/**
 * @typedef type_send_image_option
 * @property {boolean} [is_message_format_markdown]
 * @property {boolean} [is_keyboard_remove]
 * @property {boolean} [is_reply_to_message_from_notice]
 * @property {boolean} [cashe_message]
 */

/**
 * @typedef type_send_button
 * @property {string} text
 * @property {string|Object} [callback_data]
 * @property {boolean} [cashe_callback_data]
 */

/**
 * @typedef type_send_queue_message
 * @property {string} message
 * @property {Array.<Array.<type_send_button>>} [buttons]
 * @property {type_send_message_option} [option]
 */

/**
 * @private
 * @typedef type_message_edit
 * @property {boolean} [buttons_is_edit]
 * @property {Array.<Array.<type_send_button>>} [buttons]
 * @property {boolean} [message_is_edit]
 * @property {string} [message]
 * @property {type_send_message_option} [option]
*/

/** @private */
const lib_util = require('util')
/** @private */
const lib_event = require('events').EventEmitter
/** @private */
const lib_vconv = require('viva-convert')
/** @private */
const lib_telegraf = require('telegraf')
/** @private */
const lib_path = require('path')
/** @private */
const lib_fs = require('fs-extra')
/** @private */
const lib_tzlookup = require('tz-lookup')
/** @private */
const lib_tzmoment = require('moment-timezone')
/** @private */
const lib_cache = require('./cashe.js')


module.exports = Telegram
lib_util.inherits(Telegram, lib_event)
Telegram.prototype.emit = Telegram.prototype.emit || undefined
Telegram.prototype.on = Telegram.prototype.on || undefined
Telegram.prototype.bot = undefined

/** @type {string} */
Telegram.prototype.bot_name = 'unknown_bot_name'
/** @private @type {number} */
Telegram.prototype.cashe_callback_data_timeout = undefined
/** @type {lib_cache} */
Telegram.prototype.cache = undefined

/**
* @class  (license MIT) library for work with telegram, full example - see example.js
*/
function Telegram() {
    if (!(this instanceof Telegram)) return new Telegram()
    //TODO wtf? in one comp error, in another comp no error (
    // @ts-ignore
    lib_event.call(this)
}

/**
 * escaping service markdown characters
 * @param {string} text
 * @returns {string}
 */
Telegram.prototype.message_text_markdown_escape = function (text) {
    let t = lib_vconv.toString(text,'')
    if (lib_vconv.isEmpty(t)) {
        return t
    }
    t = lib_vconv.replaceAll(t,'_','\\_')
    t = lib_vconv.replaceAll(t,'*','\\*')
    return t
}

/**
 * main function for launch bot
 * @param {string} token
 * @param {type_option} [option]
 * @param {Function} callback error
 */
Telegram.prototype.launch = function (token, option, callback) {
    if (!lib_vconv.isFunction(callback)) {
        throw new Error('telegram.launch(..., callback) - callback is not function')
    }
    try {
        if (lib_vconv.isEmpty(token)) {
            callback (new Error('token is empty'))
            return
        }

        this.cache = new lib_cache()
        this.cache.init(
            this,
            {
                mode: (lib_vconv.isAbsent(option) ? undefined : lib_vconv.toString(option.cashe_callback_data, 'manual')),
                path: (lib_vconv.isAbsent(option) ? '' : lib_vconv.toString(option.cashe_callback_data_path,'')),
                allow_bot_name_in_path: (lib_vconv.isAbsent(option) ? true : lib_vconv.toBool(option.cashe_callback_data_path_bot_name, true)),
                timelive: (lib_vconv.isAbsent(option) ? undefined : lib_vconv.toInt(option.cashe_callback_data_timelive))
            },
            {
                mode: (lib_vconv.isAbsent(option) ? undefined : lib_vconv.toBool(option.cashe_message, false)),
                path: (lib_vconv.isAbsent(option) ? '' : lib_vconv.toString(option.cashe_message_path,'')),
                allow_bot_name_in_path: (lib_vconv.isAbsent(option) ? true : lib_vconv.toBool(option.cashe_message_path_bot_name, true)),
                timelive: (lib_vconv.isAbsent(option) ? undefined : lib_vconv.toInt(option.cashe_message_timelive))
            }
        )
        this.cache.on('error', error => {
            this.emit('error', error)
        })

        // @ts-ignore
        this.bot = new lib_telegraf(token)
        this.bot.catch((error) => {
            this.emit('error', error)
        })

        this.bot.use((ctx, next) => {
            notice_build(this, ctx, (error, notice) => {
                if (!lib_vconv.isEmpty(error)) {
                    this.emit('error', error)
                } else {
                    this.emit('notice', notice)
                }
            })
        })

        this.bot
            .launch()
            .then(result => {
                if (lib_vconv.isAbsent(this.bot.context) || lib_vconv.isAbsent(this.bot.context.botInfo)) {
                    callback(new Error ('error connect to telegram'))
                    return
                }
                this.bot_name = this.bot.context.botInfo.username
                callback (undefined)
            })
            .catch(error => {
                callback (error)
            })
    } catch (error) {
        callback (error)
    }
}

/**
 * send message and create keyboard, located under the text entry field
 * @param {type_notice} notice
 * @param {string} message
 * @param {Array.<Array.<type_send_keyboardmenu_option>>} keyboard
 * @param {type_send_keyboard_option} [option]
 * @param {Function} [callback] message_id - number
 */
Telegram.prototype.keyboard = function (notice, message, keyboard, option, callback) {
    try {
        let k = []
        if (!lib_vconv.isAbsent(keyboard) && Array.isArray(keyboard) === true && keyboard.length > 0) {
            keyboard.forEach(line => {
                k.push([])
                line.forEach(button => {
                    k[k.length - 1].push({
                        text: button.text,
                        request_contact: button.is_request_contact,
                        request_location: button.is_request_location
                    })
                })
            })
        } else {
            k = undefined
        }
        send_message_core(this, notice, message, {
            parse_mode: (!lib_vconv.isAbsent(option) && option.is_caption_format_markdown === true ? 'Markdown' : undefined),
            reply_markup: JSON.stringify({
                keyboard: k,
                resize_keyboard: true,
                one_time_keyboard: (!lib_vconv.isAbsent(option) && option.is_keyboard_hide_after_click === false ? false : true),
            }),
            reply_to_message_id: (!lib_vconv.isAbsent(option) && option.is_reply_to_message_from_notice === true && !lib_vconv.isEmpty(notice.message_id) ? notice.message_id : undefined)
        }, callback)
    } catch (error) {
        if (lib_vconv.isFunction(callback)) {
            callback(undefined)
        }
        this.emit('error', error, notice)
    }
}

// /**
//  * remove keyboard located under the text entry field
//  * @param {type_notice} notice
//  * @param {string} caption
//  * @param {type_send_keyboard_option} [option]
//  * @param {Function} [callback] message_id - number
//  */
// Telegram.prototype.keyboard_remove = function (notice, caption, option, callback) {
//     try {
//         send_message_core(this, notice, caption, {
//             parse_mode: (!lib_vconv.isAbsent(option) && option.is_caption_format_markdown === true ? 'Markdown' : undefined),
//             reply_markup: {remove_keyboard: true}
//         }, callback)
//     } catch (error) {
//         if (lib_vconv.isFunction(callback)) {
//             callback(undefined)
//         }
//         this.emit('error', error, notice)
//     }
// }

/**
 * send message with buttons below this message
 * @param {type_notice} notice
 * @param {string} message
 * @param {Array.<Array.<type_send_button>>} [buttons]
 * @param {type_send_message_option} [option]
 * @param {Function} [callback] message_id - number
 */
Telegram.prototype.message = function (notice, message, buttons, option, callback) {
    try {
        let true_buttons = delete_empty_buttons(buttons)
        let message_cashe = this.cache.cashe_message_build(message, buttons, option)
        let callback_data_cache = this.cache.cashe_callback_data_build(true_buttons)

        send_message_core(this, notice, message, {
            parse_mode: (!lib_vconv.isAbsent(option) && option.is_message_format_markdown === true ? 'Markdown' : undefined),
            reply_markup: JSON.stringify({
                inline_keyboard: true_buttons,
                remove_keyboard: (!lib_vconv.isAbsent(option) && option.is_keyboard_remove === true ? true : false)
            }),
            reply_to_message_id: (!lib_vconv.isAbsent(option) && option.is_reply_to_message_from_notice === true && !lib_vconv.isEmpty(notice.message_id) ? notice.message_id : undefined)
        }, message_id => {
            this.cache.save(notice.account, message_id, message_cashe, callback_data_cache, error => {
                if (!lib_vconv.isEmpty(error)) {
                    message_id = undefined
                    this.emit('error', error, notice)
                }
                if (lib_vconv.isFunction(callback)) {
                    callback(message_id)
                }
            })
        })
    } catch (error) {
        if (lib_vconv.isFunction(callback)) {
            callback(undefined)
        }
        this.emit('error', error, notice)
    }
}

/**
 * send image with buttons below this message
 * @param {type_notice} notice
 * @param {Buffer} image
 * @param {string} [message]
 * @param {Array.<Array.<type_send_button>>} [buttons]
 * @param {type_send_image_option} [option]
 * @param {Function} [callback] message_id - number
 */
Telegram.prototype.image = function (notice, image, message, buttons, option, callback) {
    try {
        let true_buttons = delete_empty_buttons(buttons)
        let message_cashe = this.cache.cashe_message_build(message, buttons, option, image)
        let callback_data_cache = this.cache.cashe_callback_data_build(true_buttons)

        send_image_core(this, notice, image, {
            parse_mode: (!lib_vconv.isAbsent(option) && option.is_message_format_markdown === true ? 'Markdown' : undefined),
            caption: message,
            reply_markup: JSON.stringify({
                inline_keyboard: true_buttons,
                remove_keyboard: (!lib_vconv.isAbsent(option) && option.is_keyboard_remove === true ? true : false)
            }),
            reply_to_message_id: (!lib_vconv.isAbsent(option) && option.is_reply_to_message_from_notice === true && !lib_vconv.isEmpty(notice.message_id) ? notice.message_id : undefined)
        }, message_id => {
            this.cache.save(notice.account, message_id, message_cashe, callback_data_cache, error => {
                if (!lib_vconv.isEmpty(error)) {
                    message_id = undefined
                    this.emit('error', error, notice)
                }
                if (lib_vconv.isFunction(callback)) {
                    callback(message_id)
                }
            })
        })
    } catch (error) {
        if (lib_vconv.isFunction(callback)) {
            callback(undefined)
        }
        this.emit('error', error, notice)
    }
}

/**
 * delete previously sent message or image
 * @param {type_notice} notice
 * @param {number} message_id
 * @param {Function} [callback] is_deleted - boolean
 */
Telegram.prototype.delete = function (notice, message_id, callback) {
    try {
        if (lib_vconv.isAbsent(notice) || lib_vconv.isAbsent(notice._ctx) || lib_vconv.isAbsent(message_id)) {
            callback(false)
            return
        }
        notice._ctx
            .deleteMessage(message_id)
            .then(result => {
                if (lib_vconv.isFunction(callback) && !lib_vconv.isAbsent(result)) {
                    callback(result)
                }
            })
            .catch(error => {
                if (lib_vconv.isFunction(callback)) {
                    callback(false)
                }
                this.emit('error', error, notice)
            })
    } catch (error) {
        if (lib_vconv.isFunction(callback)) {
            callback(undefined)
        }
        this.emit('error', error, notice)
    }
}

/**
 * from cashe load previously saved message or image
 * @param {string} account
 * @param {number} message_id
 * @param {function} callback (error, result = {message, buttons, option, image})
 */
Telegram.prototype.load_from_cashe = function (account, message_id, callback) {
    if (!lib_vconv.isFunction(callback)) {
        throw new Error('callback is not function')
    }
    this.cache.cashe_message_load(account, message_id, callback)
}

// /**
//  * @private
//  * @param {number} chat_id
//  * @param {string} account
//  * @param {number} message_id
//  * @param {Array.<Array.<type_send_button>>} buttons
//  * @param {function} [callback] (result - true or false)
//  */
// Telegram.prototype.message_buttons_edit = function (chat_id, account, message_id, buttons, callback) {
//     let true_buttons = delete_empty_buttons(buttons)
//     let callback_data_cache = this.cache.cashe_callback_data_build(true_buttons)
//     let message_cashe = undefined
//     this.message_load(account, message_id, (error, result) => {
//         if (!lib_vconv.isAbsent(error)) {
//             this.emit('error', error)
//         } else {
//             if (!lib_vconv.isAbsent(result)) {
//                 message_cashe = this.cache.cashe_message_build(result.message, true_buttons, result.option, result.image)
//             }
//         }
//         this.bot.telegram
//         .editMessageReplyMarkup(chat_id, message_id, undefined, JSON.stringify({inline_keyboard: true_buttons}))
//         .then(result => {
//             this.cache.save(account, message_id, message_cashe, callback_data_cache, error => {
//                 if (!lib_vconv.isEmpty(error)) {
//                     this.emit('error', error)
//                     if (lib_vconv.isFunction(callback)) {
//                         callback(false)
//                     }
//                 } else {
//                     if (lib_vconv.isFunction(callback)) {
//                         callback(true)
//                     }
//                 }
//             })
//         })
//         .catch(error => {
//             if (lib_vconv.isFunction(callback)) {
//                 callback(false)
//             }
//             this.emit('error', error)
//         })
//     })
// }

/**
 * edit previously sent message
 * @private
 * @param {number} chat_id
 * @param {string} account
 * @param {number} message_id
 * @param {type_message_edit} edit
 * @param {function} [callback] (result - true or false)
 */
Telegram.prototype.message_edit = function (chat_id, account, message_id, edit, callback) {
    if (lib_vconv.isAbsent(chat_id) || lib_vconv.isEmpty(account) || lib_vconv.isAbsent(message_id) || lib_vconv.isAbsent(edit)) {
        if (lib_vconv.isFunction(callback)) {
            callback(false)
        }
        return
    }
    message_edit_preprocessor(this, account, message_id, edit, (callback_error, message, true_buttons, option, message_cashe, callback_data_cache) => {
        if (!lib_vconv.isAbsent(callback_error)) {
            this.emit('error', callback_error)
        }

        this.bot.telegram
        .editMessageText(chat_id, message_id, undefined, message,
            {
                parse_mode: (!lib_vconv.isAbsent(option) && option.is_message_format_markdown === true ? 'Markdown' : undefined),
                reply_markup: JSON.stringify({
                    inline_keyboard: true_buttons
                })
            }
        )
        .then(result => {
            this.cache.save(account, message_id, message_cashe, callback_data_cache, error => {
                if (!lib_vconv.isEmpty(error)) {
                    this.emit('error', error)
                    if (lib_vconv.isFunction(callback)) {
                        callback(false)
                    }
                } else {
                    if (lib_vconv.isFunction(callback)) {
                        callback(true)
                    }
                }
            })
        })
        .catch(error => {
            if (lib_vconv.isFunction(callback)) {
                callback(false)
            }
            this.emit('error', error)
        })
    })
}

/**
 * edit previously sent image (caption or buttons)
 * @private
 * @param {number} chat_id
 * @param {string} account
 * @param {number} message_id
 * @param {type_message_edit} edit
 * @param {function} [callback] (result - true or false)
 */
Telegram.prototype.image_edit = function (chat_id, account, message_id, edit, callback) {
    if (lib_vconv.isAbsent(chat_id) || lib_vconv.isEmpty(account) || lib_vconv.isAbsent(message_id) || lib_vconv.isAbsent(edit)) {
        if (lib_vconv.isFunction(callback)) {
            callback(false)
        }
        return
    }
    message_edit_preprocessor(this, account, message_id, edit, (callback_error, message, true_buttons, option, message_cashe, callback_data_cache) => {
        if (!lib_vconv.isAbsent(callback_error)) {
            this.emit('error', callback_error)
        }

        this.bot.telegram
        .editMessageCaption(chat_id, message_id, undefined, message,
            {
                parse_mode: (!lib_vconv.isAbsent(option) && option.is_message_format_markdown === true ? 'Markdown' : undefined),
                reply_markup: JSON.stringify({
                    inline_keyboard: true_buttons
                })
            }
        )
        .then(result => {
            this.cache.save(account, message_id, message_cashe, callback_data_cache, error => {
                if (!lib_vconv.isEmpty(error)) {
                    this.emit('error', error)
                    if (lib_vconv.isFunction(callback)) {
                        callback(false)
                    }
                } else {
                    if (lib_vconv.isFunction(callback)) {
                        callback(true)
                    }
                }
            })
        })
        .catch(error => {
            if (lib_vconv.isFunction(callback)) {
                callback(false)
            }
            this.emit('error', error)
        })
    })
}

/**
 * send many messages
 * @param {type_notice} notice
 * @param {type_send_queue_message[]} items
 * @param {Function} [callback] message_ids - number[]
 */
Telegram.prototype.queue = function (notice, items, callback) {
    let self = this
    /** @private @type {number[]} */
    let numbers = []

    /**
     * @private
     * @param {number} index
     * @param {type_notice} notice
     * @param {type_send_queue_message[]} items
     */
    let _queue = function(index, notice, items) {
        let message = items[index]
        if (lib_vconv.isAbsent(message)) {
            numbers.push(undefined)
            index++
            if (index < items.length) {
                _queue(index, notice, items)
            } else {
                if (lib_vconv.isFunction(callback)) {
                    callback(numbers)
                }
                _queue = undefined
            }
        } else {
            self.message(notice, message.message, message.buttons, message.option, message_id => {
                numbers.push(message_id)
                index++
                if (index < items.length) {
                    _queue(index, notice, items)
                } else {
                    if (lib_vconv.isFunction(callback)) {
                        callback(numbers)
                    }
                    _queue = undefined
                }
            })
        }
    }
    _queue(0, notice, items)
}

/**
 * @static
 * @private
 * @param {Telegram} self
 * @param {string} account
 * @param {number} message_id
 * @param {type_message_edit} edit
 * @param {function} callback (error, result - callback_error, message, true_buttons, option, message_cashe, callback_data_cache)
 */
function message_edit_preprocessor (self, account, message_id, edit, callback) {
    if (!lib_vconv.isFunction(callback)) {
        throw new Error('callback is not function')
    }
    try {
        let callback_error = undefined
        if (lib_vconv.isAbsent(edit)) {
            edit = {
                buttons: undefined,
                message: undefined,
                option: undefined,
                buttons_is_edit: false,
                message_is_edit: false
            }
        }

        self.load_from_cashe(account, message_id, (error, result) => {
            if (!lib_vconv.isAbsent(error)) {
                callback_error = error
            }
            if (lib_vconv.isAbsent(result)) {
                result = {}
            }

            let message = (edit.message_is_edit === true ? edit.message : result.message)
            let true_buttons = delete_empty_buttons(edit.buttons_is_edit === true ? edit.buttons : result.buttons)
            let option = (lib_vconv.isAbsent(edit.option) ? result.option : edit.option)
            let message_cashe = self.cache.cashe_message_build(message, true_buttons, option, result.image)
            let callback_data_cache = self.cache.cashe_callback_data_build(true_buttons)

            callback(callback_error, message, true_buttons, option, message_cashe, callback_data_cache)
        })
    } catch (error) {
        callback(error)
    }
}


/**
 * @static
 * @private
 * @param {Telegram} self
 * @param {Object} ctx
 * @param {Function} callback (error, notice)
 */
function notice_build (self, ctx, callback) {
    if (!lib_vconv.isFunction(callback)) {
        throw new Error('telegram.notice_build(..., callback) - callback is not function')
    }
    try {

        if (lib_vconv.isAbsent(ctx)) {
            callback(undefined, undefined)
            return
        }

        let chat_id
        if (!lib_vconv.isAbsent(ctx.chat)) {
            chat_id = lib_vconv.toInt(ctx.chat.id)
        }

        let message_id
        if (!lib_vconv.isAbsent(ctx.message)) {
            message_id = lib_vconv.toInt(ctx.message.message_id)
        } else if (!lib_vconv.isAbsent(ctx.callbackQuery) && !lib_vconv.isAbsent(ctx.callbackQuery.message)) {
            message_id = lib_vconv.toInt(ctx.callbackQuery.message.message_id)
        }

        let location_latitude
        let location_longitude
        let utc_offset
        let utc_offset_name
        if (!lib_vconv.isAbsent(ctx.message) && !lib_vconv.isAbsent(ctx.message.location)) {
            location_latitude = lib_vconv.toFloat(ctx.message.location.latitude)
            location_longitude = lib_vconv.toFloat(ctx.message.location.longitude)
            if (lib_vconv.isAbsent(location_latitude) || lib_vconv.isAbsent(location_longitude)) {
                location_latitude = undefined
                location_longitude = undefined
            }
            if (!lib_vconv.isAbsent(location_latitude) && !lib_vconv.isAbsent(location_longitude)) {
                utc_offset_name = lib_tzlookup(location_latitude, location_longitude)
                if (!lib_vconv.isEmpty(utc_offset_name)) {
                    utc_offset = lib_tzmoment().tz(utc_offset_name).utcOffset()
                }
            }
        }

        /** @type {type_notice} */
        let ret = {
            _ctx: ctx,
            is_bot: (lib_vconv.isAbsent(ctx.from) ? true : lib_vconv.toBool(ctx.from.is_bot, true)),
            account: (lib_vconv.isAbsent(ctx.from) ? '' : lib_vconv.toString(ctx.from.id,'')),
            language: (lib_vconv.isAbsent(ctx.from) ? undefined : ctx.from.language_code),
            chat_id: chat_id,
            message_id: message_id,
            message: (lib_vconv.isAbsent(ctx.message) ? '' : lib_vconv.toString(ctx.message.text,'').trim()),
            callback_data: (lib_vconv.isAbsent(ctx.callbackQuery) ? undefined : ctx.callbackQuery.data),
            location_latitude: location_latitude,
            location_longitude: location_longitude,
            utc_offset: utc_offset,
            utc_offset_name: utc_offset_name,
            need_upload_document: (lib_vconv.isAbsent(ctx.message) || lib_vconv.isAbsent(ctx.message.document) || lib_vconv.isAbsent(ctx.message.document.file_id) ? undefined : {
                file_id: ctx.message.document.file_id,
                file_name: ctx.message.document.file_name,
                file_size: ctx.message.document.file_size,
                mime_type: ctx.message.document.mime_type
            })
        }

        self.cache.cashe_callback_data_load(ret.account, message_id, ret.callback_data, (error, result) => {
            if (!lib_vconv.isEmpty(error)) {
                callback(error)
            }
            ret.callback_data = result
            callback(undefined, ret)
        })
    } catch (error) {
        callback(error)
    }
}

/**
 * @static
 * @private
 * @param {Telegram} self
 * @param {type_notice} notice
 * @param {string} message
 * @param {type_send_text_option} [option]
 * @param {Function} [callback] message_id - number
 */
function send_message_core (self, notice, message, option, callback) {
    self.bot.telegram
    .sendMessage(notice.account, message, option)
    .then(result => {
        if (lib_vconv.isFunction(callback) && !lib_vconv.isAbsent(result)) {
            callback(result.message_id)
        }
    })
    .catch(error => {
        if (lib_vconv.isFunction(callback)) {
            callback(undefined)
        }
        if (error.code === 429 && !lib_vconv.isAbsent(error.parameters) && lib_vconv.toInt(error.parameters.retry_after, 0) > 0) {
            self.emit('over', lib_vconv.toInt(error.parameters.retry_after), notice)
        } else {
            self.emit('error', error, notice)
        }
    })
}

/**
 * @static
 * @private
 * @param {Telegram} self
 * @param {type_notice} notice
 * @param {Buffer} image
 * @param {type_send_photo_option} [option]
 * @param {Function} [callback] message_id - number
 */
function send_image_core (self, notice, image, option, callback) {
    self.bot.telegram
    .sendPhoto(notice.account, { source: image }, option)
    .then(result => {
        if (lib_vconv.isFunction(callback) && !lib_vconv.isAbsent(result)) {
            callback(result.message_id)
        }
    })
    .catch(error => {
        if (lib_vconv.isFunction(callback)) {
            callback(undefined)
        }
        self.emit('error', error, notice)
    })
}

/**
 * @static
 * @private
 * @param {Array.<Array.<type_send_button>>} [buttons]
 * @returns {Array.<Array.<type_send_button>>}
 */
function delete_empty_buttons (buttons) {
    if (lib_vconv.isAbsent(buttons)) {
        return undefined
    }
    let true_buttons = []
    let true_buttons_line = []
    buttons.filter(f => !lib_vconv.isAbsent(f)).forEach(b1 => {
        b1.filter(f => !lib_vconv.isAbsent(f)).forEach(b2 => {
            true_buttons_line.push(b2)
        })
        if (true_buttons_line.length > 0) {
            true_buttons.push(true_buttons_line)
            true_buttons_line = []
        }
    })
    return true_buttons
}