let lib = require('viva-telegram')
let telegram = new lib()
let path = require('path')

telegram.on('error', (error) => {
    console.error(error)
})

telegram.on('over', (retry_after, notice) => {
    telegram.message(notice, 'retry after '.concat(retry_after,' sec'))
})

telegram.on('notice', notice => {
    console.log(notice)
    // for intellisense in notice
    on_get_message(notice)
})
/**
 * @param {lib.type_notice} notice
 */
function on_get_message(notice) {
    if (notice.message === '/start') {
        telegram.message(notice, 'Choose demo', [
            [{text: 'keyboard', callback_data: 'demo_keyboard'}],
            [{text: 'message and buttons', callback_data: 'demo_buttons1'}],
            [{text: 'image and buttons', callback_data: 'demo_buttons2'}],
        ], {is_reply_to_message_from_notice: true})
        return
    }

    if (notice.message === 'remove keyboard') {
        telegram.message(notice, 'I remove keyboard', undefined, {is_keyboard_remove: true, is_reply_to_message_from_notice: true})
        return
    }

    if (notice.callback_data === 'demo_keyboard') {
        telegram.keyboard(notice, 'I create keyboard!',[
            [{text: 'button1'}, {text: 'button2'}, {text: 'button3'}],
            [{text: 'button4'}, {text: 'button5'}],
            [{text: 'remove keyboard'}]
        ], undefined)
        return
    }

    if (notice.callback_data === 'demo_buttons1') {
        telegram.message(
            notice,
            'This is message with buttons. *First button* with large callback data, *second button* remove first button from this message, *next button* remove this message, *this button* add to this text "bla-bla-bla"',
            [
                [{text: 'first button', callback_data: 'very large callback data for example cashed callback data subsystem'}],
                [{text: 'second button', callback_data: 'msg_button_shift'}],
                [{text: 'next button', callback_data: 'remove'}],
                [{text: 'this button', callback_data: 'msg_message_edit'}],
                [{text: 'button1', callback_data: 'button1'}, {text: 'button2', callback_data: 'button2'}]
            ],
            {cashe_message: true, is_message_format_markdown: true}
        )
        return
    }

    if (notice.callback_data === 'demo_buttons2') {
        telegram.image(
            notice,
            Buffer.from('iVBORw0KGgoAAAANSUhEUgAAAFoAAABaCAYAAAA4qEECAAAAkklEQVR42u3QMQ0AMAgAsOFg5vGIC1BBwtFKaPysfqwL0aJFI1q0aNGiRSNatGjRokUjWrRo0aJFI1q0aA2iRSNatGhEi0a0aNGIFo1o0aIRLRrRokUjWjSiRYtGtGhEixaNaNGIFi0a0aIRLVo0okUjWrRoRItGtGjRiBaNaNGiES0a0aJFI1o0okWLRrRoRF8xy3/lk3nU9eoAAAAASUVORK5CYII=','base64'),
            'This is blue square with caption and buttons. *First button* with large callback data, *second button* remove first button from this message, *next button* remove this message, *this button* add to this text "bla-bla-bla"',
            [
                [{text: 'first button', callback_data: 'very large callback data for example cashed callback data subsystem'}],
                [{text: 'second button', callback_data: 'img_button_shift'}],
                [{text: 'next button', callback_data: 'remove'}],
                [{text: 'this button', callback_data: 'img_message_edit'}],
                [{text: 'button1', callback_data: 'button1'}, {text: 'button2', callback_data: 'button2'}]
            ],
            {cashe_message: true, is_message_format_markdown: true}
        )
        return
    }

    if (notice.callback_data === 'very large callback data for example cashed callback data subsystem') {
        telegram.message(
            notice,
            notice.callback_data,
            undefined,
            {is_reply_to_message_from_notice: true}
        )
        return
    }

    if (notice.callback_data === 'msg_button_shift') {
        telegram.load_from_cashe(notice.account, notice.message_id, (error, result) => {
            if (error) {
                console.log(error)
                return
            }
            result.buttons.shift()
            telegram.message_edit(notice.chat_id, notice.account, notice.message_id, {
                buttons_is_edit: true,
                buttons: result.buttons
            })
        })
        return
    }

    if (notice.callback_data === 'img_button_shift') {
        telegram.load_from_cashe(notice.account, notice.message_id, (error, result) => {
            if (error) {
                console.log(error)
                return
            }
            result.buttons.shift()
            telegram.image_edit(notice.chat_id, notice.account, notice.message_id, {
                buttons_is_edit: true,
                buttons: result.buttons
            })
        })
        return
    }

    if (notice.callback_data === 'remove') {
        telegram.delete(notice, notice.message_id)
        return
    }

    if (notice.callback_data === 'msg_message_edit') {
        telegram.load_from_cashe(notice.account, notice.message_id, (error, result) => {
            if (error) {
                console.log(error)
                return
            }
            telegram.message_edit(notice.chat_id, notice.account, notice.message_id, {
                message_is_edit: true,
                message: result.message.concat(' bla-bla-bla')
            })
        })
        return
    }

    if (notice.callback_data === 'img_message_edit') {
        telegram.load_from_cashe(notice.account, notice.message_id, (error, result) => {
            if (error) {
                console.log(error)
                return
            }
            telegram.image_edit(notice.chat_id, notice.account, notice.message_id, {
                message_is_edit: true,
                message: result.message.concat(' bla-bla-bla')
            })
        })
        return
    }

    telegram.message(notice, 'I don\'t understand you, *sorry*, send /start command', undefined, {is_message_format_markdown: true, is_reply_to_message_from_notice: true} )
}

telegram.launch('YOUR TOKEN', {
    cashe_callback_data: 'auto',
    cashe_callback_data_path: path.join(__dirname, 'cashe'),
    cashe_callback_data_path_bot_name: true,
    cashe_callback_data_timelive: 60,
    cashe_message: false,
    cashe_message_path: path.join(__dirname, 'cashe'),
    cashe_message_path_bot_name: true,
    cashe_message_timelive: 60
} , error => {
    if (error) {
        console.log(error)
        return
    }
})